from FUNcubeStatusMonitor import FUNcubeStatusMonitor

import mock
import unittest

TEST_JSON = {'minmaxReset': '2019-04-14 00:14:37 UTC', 'packetCount': '7220659 (1848.5MB)',
             'paValues': [{'min': 'N/A', 'name': 'Forward Power', 'max': 'N/A', 'value': '349.4 mW'},
                          {'min': 'N/A', 'name': 'Reverse Power', 'max': 'N/A', 'value': '46.6 mW'},
                          {'min': '  37', 'name': 'Device Temperature', 'max': '  42', 'value': '38.3 C'},
                          {'min': ' 103', 'name': 'Bus Current', 'max': ' 110', 'value': '109.7 mA'}],
             'swValues': [{'name': 'Sequence Number', 'value': '   1423340'},
                          {'name': 'Command Count', 'value': '         2'},
                          {'name': 'Last Command', 'value': 'CPLD set power on mode'},
                          {'name': 'Command Success', 'value': 'No'}, {'name': 'Data Valid ASIB', 'value': 'Yes'},
                          {'name': 'Data Valid EPS', 'value': 'Yes'}, {'name': 'Data Valid PA', 'value': 'Yes'},
                          {'name': 'Data Valid RF', 'value': 'Yes'}, {'name': 'Data Valid MSE', 'value': 'Yes'},
                          {'name': 'Data Valid ANTS Bus-B', 'value': 'Yes'},
                          {'name': 'Data Valid ANTS Bus-A', 'value': 'Yes'}, {'name': 'In Eclipse Mode', 'value': 'No'},
                          {'name': 'In Safe Mode', 'value': 'No'}, {'name': 'Hardware ABF', 'value': 'On'},
                          {'name': 'Software ABF', 'value': 'Off'},
                          {'name': 'Deployment Wait At Next Boot', 'value': 'No'}], 'latitude': ' 54.6 N',
             'longitude': ' 55.8 E', 'valid': 1, 'transponderState': 'Off',
             'asibValues': [{'min': 'N/A', 'name': 'Sun Sensor X+', 'max': 'N/A', 'value': '2.94'},
                            {'min': 'N/A', 'name': 'Sun Sensor Y+', 'max': 'N/A', 'value': '4.41'},
                            {'min': 'N/A', 'name': 'Sun Sensor Z+', 'max': 'N/A', 'value': '3.10'},
                            {'min': ' 16.0', 'name': 'Solar Panel Temp X+', 'max': ' 26.2', 'value': ' 19.1 C'},
                            {'min': ' 17.4', 'name': 'Solar Panel Temp X-', 'max': ' 26.7', 'value': ' 18.2 C'},
                            {'min': ' 16.4', 'name': 'Solar Panel Temp Y+', 'max': ' 26.2', 'value': ' 18.3 C'},
                            {'min': ' 16.3', 'name': 'Solar Panel Temp Y-', 'max': ' 26.1', 'value': ' 18.0 C'},
                            {'min': '3276', 'name': '3.3 Bus Voltage', 'max': '3280', 'value': '3280 mV'},
                            {'min': ' 113', 'name': '3.3 Bus Current', 'max': ' 154', 'value': ' 151 mA'},
                            {'min': '4956', 'name': '5.0 Bus voltage', 'max': '4962', 'value': '4962 mV'}],
             'epsValues': [{'min': '2951', 'name': 'Solar Panel Voltage X', 'max': '4874', 'value': '4471 mV'},
                           {'min': '2409', 'name': 'Solar Panel Voltage Y', 'max': '4846', 'value': '4444 mV'},
                           {'min': '1559', 'name': 'Solar Panel Voltage Z', 'max': '4829', 'value': '4354 mV'},
                           {'min': '   2', 'name': 'Total Photo Current', 'max': ' 362', 'value': ' 217 mA'},
                           {'min': '8242', 'name': 'Battery Voltage', 'max': '8313', 'value': '8282 mV'},
                           {'min': ' 196', 'name': 'Total System Current', 'max': ' 233', 'value': ' 219 mA'},
                           {'min': 'N/A', 'name': 'Reboot Count', 'max': 'N/A', 'value': '1336'},
                           {'min': 'N/A', 'name': 'EPS Software Errors', 'max': 'N/A', 'value': '   0'},
                           {'min': '  20', 'name': 'Boost Converter Temp X', 'max': '  26', 'value': '  21 C'},
                           {'min': '  20', 'name': 'Boost Converter Temp Y', 'max': '  26', 'value': '  21 C'},
                           {'min': '  20', 'name': 'Boost Converter Temp Z', 'max': '  26', 'value': '  22 C'},
                           {'min': '  20', 'name': 'Battery Temp', 'max': '  25', 'value': '  21 C'},
                           {'min': 'N/A', 'name': 'Latch Up Count 5v1', 'max': 'N/A', 'value': '   0'},
                           {'min': 'N/A', 'name': 'Latch Up Count 3.3v1', 'max': 'N/A', 'value': '   0'},
                           {'min': 'N/A', 'name': 'Reset Cause', 'max': 'N/A', 'value': '   3'},
                           {'min': 'N/A', 'name': 'Power Point Tracking Mode', 'max': 'N/A', 'value': '   1'}],
             'lastUpdated': '2019-04-17 14:19:05 UTC',
             'rfValues': [{'min': 'N/A', 'name': 'Receiver Doppler', 'max': 'N/A', 'value': ' 162'},
                          {'min': 'N/A', 'name': 'Receiver RSSI', 'max': 'N/A', 'value': ' 182'},
                          {'min': ' 22.3', 'name': 'Temperature', 'max': ' 27.4', 'value': ' 23.1 C'},
                          {'min': '  40', 'name': 'Receive Current', 'max': '  41', 'value': '  40 mA'},
                          {'min': '  50', 'name': 'Transmit Current 3.3V bus', 'max': '  52', 'value': '  51 mA'},
                          {'min': '  24', 'name': 'Transmit Current 5.0V bus', 'max': '  24', 'value': '  24 mA'}],
             'siteList': ['OH8MBN'], 'sequenceNumber': 1423340, 'satelliteMode': 'Manual',
             'antsValues': [{'min': '  8.1 C', 'name': 'Antenna Temp 0', 'max': ' 22.5 C', 'value': ' 15.3 C'},
                            {'min': '  6.9 C', 'name': 'Antenna Temp 1', 'max': ' 22.5 C', 'value': ' 14.1 C'},
                            {'min': '', 'name': 'Antenna Deployment VHF-A', 'max': '', 'value': 'Deployed'},
                            {'min': '', 'name': 'Antenna Deployment UHF-A', 'max': '', 'value': 'Deployed'},
                            {'min': '', 'name': 'Antenna Deployment VHF-B', 'max': '', 'value': 'Deployed'},
                            {'min': '', 'name': 'Antenna Deployment UHF-B', 'max': '', 'value': 'Deployed'}]}


class test_status_monitor(unittest.TestCase):

    class TestTimeline:

        def __init__(self, tweet_text):
            self.tweet_text = tweet_text

        @property
        def text(self):
            return self.tweet_text

    @mock.patch('FUNcubeStatusMonitor.urllib2', mock.MagicMock(return_value=None))
    @mock.patch('FUNcubeStatusMonitor.json.loads', mock.MagicMock(return_value=TEST_JSON))
    def test_update_status(self):

        with mock.patch("FUNcubeStatusMonitor.tweepy.API") as MockTweepy:

            MockTweepy.return_value.me.return_value.name = "test-name"
            MockTweepy.return_value.me.return_value.location = "test-location"
            MockTweepy.return_value.me.return_value.followers_count = "1234"

            with self.subTest("No change"):

                MockTweepy.return_value.user_timeline.return_value = [test_status_monitor.TestTimeline(
                        "FUNcube-1 status update: Mode MANUAL. Transponder OFF. #FUNcube #amsat #hamradio")]

                ref = FUNcubeStatusMonitor()
                self.assertFalse(ref.update_status())

            with self.subTest("Change of mode"):
                MockTweepy.return_value.user_timeline.return_value = [test_status_monitor.TestTimeline(
                        "FUNcube-1 status update: Mode AUTO. Transponder OFF. #FUNcube #amsat #hamradio")]

                ref = FUNcubeStatusMonitor()
                self.assertTrue(ref.update_status())

            with self.subTest("Change of transponder status"):
                MockTweepy.return_value.user_timeline.return_value = [test_status_monitor.TestTimeline(
                        "FUNcube-1 status update: Mode MANUAL. Transponder ON. #FUNcube #amsat #hamradio")]

                ref = FUNcubeStatusMonitor()
                self.assertTrue(ref.update_status())

            with self.subTest("Change of transponder status"):
                MockTweepy.return_value.user_timeline.return_value = [test_status_monitor.TestTimeline("blah")]

                ref = FUNcubeStatusMonitor()

                with self.assertRaises(SystemExit):
                    ref.update_status()
