# Python script to monitor FUNcube-1 status and update the 
# @FUNcubeUK Twitter account on changes

import os
import re
import json
import urllib.request as urllib2
from sys import exit
import tweepy
from dotenv import load_dotenv


class FUNcubeStatusMonitor:

    WAREHOUSE_REALTIME_URL = "http://warehouse.funcube.org.uk/ui/realtime/2.json"

    def __init__(self):

        self.sat_mode = None
        self.transponder_state = None
        self.recent_tweets = None

        # Load consumer keys and access tokens from .env, used for OAuth
        load_dotenv(dotenv_path='.env')

        # OAuth process, using the keys and tokens
        if "CONSUMER_KEY" not in os.environ:
            raise RuntimeError("Environment variables not loaded from required .dotenv file")

        auth = tweepy.OAuthHandler(os.getenv("CONSUMER_KEY"), os.getenv("CONSUMER_SECRET"))
        access_token = "{}-{}".format(os.getenv("ACCESS_TOKEN1"), os.getenv("ACCESS_TOKEN2"))
        auth.set_access_token(access_token, os.getenv("ACCESS_TOKEN_SECRET"))

        # Creation of the actual interface, using authentication
        self.twitter_api = tweepy.API(auth)

        self.twitter_user = self.twitter_api.me()

        self.PreviousSatMode = ""
        self.PreviousTransponderState = ""

        print('Name: ' + self.twitter_user.name)
        print('Location: ' + self.twitter_user.location)
        print('Followers: ' + str(self.twitter_user.followers_count))

    def _load_real_time_data(self):

        try:
            jsonurl = urllib2.urlopen(self.WAREHOUSE_REALTIME_URL)

        except urllib2.HTTPError as err:
            if err.code == 404:
                raise RuntimeError("404 not found")
            else:
                raise RuntimeError("Unknown error. Error code:", err.code)

        self.real_time_data_text = json.loads(jsonurl.read().decode('utf-8'))
        print(self.real_time_data_text)

    def _get_sat_mode(self) -> str:
        return self.real_time_data_text["satelliteMode"].upper()

    def _get_transponder_state(self) -> str:
        return self.real_time_data_text["transponderState"].upper()

    def _get_recent_tweets(self):
        return self.twitter_api.user_timeline()

    def _check_tweets(self, recent_tweets):
        print(recent_tweets)
        for recent_tweet in recent_tweets:
            if recent_tweet.text.find("FUNcube-1 status update") == 0:
                # The tweet was a status update, stop searching recent tweets
                print("Most recent status update tweet: ", recent_tweet.text)

                match_obj = re.match(r".*Mode (\w+). Transponder (\w+).", recent_tweet.text)

                if match_obj:
                    self.PreviousSatMode = match_obj.group(1)
                    self.PreviousTransponderState = match_obj.group(2)
                else:
                    print("No match!")
                    exit(0)
                break
        else:
            # Didn't find a tweet that was a status update
            print("No status update in recent tweets")
            exit(0)

    def _get_update_tweet_text(self):

        if self.sat_mode == self.PreviousSatMode and self.transponder_state == self.PreviousTransponderState:
            # If neither switching mode or transponder state have changed do nothing
            print('Satellite status unchanged - NOP')
            return
        elif self.sat_mode == "AUTO" and self.PreviousSatMode == "AUTO":
            # If in automatic mode and transponder state changes, do nothing
            print('Transponder state changed in auto mode - NOP')
            return
        else:
            print('Switching mode change or change of transponder state whilst in manual mode - UPDATING @FUNCUBEUK '
                  'STATUS')

            tweet_text = 'FUNcube-1 status update: Mode ' + self.sat_mode + '. Transponder ' \
                         + self.transponder_state + '. #FUNcube #amsat #hamradio'
            return tweet_text

    def update_status(self):
        self._load_real_time_data()
        self.sat_mode = self._get_sat_mode()
        self.transponder_state = self._get_transponder_state()
        self.recent_tweets = self._get_recent_tweets()
        self._check_tweets(self.recent_tweets)

        print("Previous Satellite Mode: ", self.PreviousSatMode)
        print("Previous Transponder Mode: ", self.PreviousTransponderState)

        tweet_text = self._get_update_tweet_text()

        if tweet_text:
            print(tweet_text)
            self.twitter_api.update_status(tweet_text)
            return True

        return False


def main():
    monitor = FUNcubeStatusMonitor()
    monitor.update_status()


if __name__ == '__main__':
    main()
